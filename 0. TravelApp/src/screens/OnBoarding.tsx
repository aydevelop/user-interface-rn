import React from 'react';
import {
  SafeAreaView,
  View,
  StyleSheet,
  Text,
  Animated,
  Image,
} from 'react-native';

import {onboarding1, onboarding2, onboarding3} from 'constants/images';
import {COLORS, FONTS, SIZES, BORDER} from 'constants/theme';

const onBoardings = [
  {
    title: 'Vestibulum sed auctor dui',
    description:
      'Pellentesque semper condimentum turpis, laoreet vestibulum leo consequat',
    img: onboarding1,
  },
  {
    title: 'Nullam diam purus, vehicula vel nunc nec',
    description:
      'Suspendisse semper risus nisi, quis congue nulla efficitur vel. Morbi porttitor iaculis risus ut viverra',
    img: onboarding2,
  },
  {
    title: 'Nam ac libero eu ante dictum convallis',
    description:
      'Sed orci orci, vulputate at feugiat eu, malesuada eget eros. Sed id fermentum metus',
    img: onboarding3,
  },
];

function renderBody(scrollX) {
  return (
    <Animated.ScrollView
      horizontal
      pagingEnabled
      scrollEnabled
      decelerationRate={0}
      snapToAlignment="center"
      showsHorizontalScrollIndicator={false}
      onScroll={Animated.event([{nativeEvent: {contentOffset: {x: scrollX}}}], {
        useNativeDriver: false,
      })}>
      {onBoardings.map((item, index) => {
        return (
          <View key={index} style={{width: SIZES.width}}>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
              }}>
              {/* <Text>{item.title}</Text> */}
              <Image
                source={item.img}
                resizeMode="cover"
                style={{width: '100%', height: '100%'}}
              />
            </View>
            <View
              style={{
                borderWidth: 1,
                borderColor: COLORS.puprle,
                paddingHorizontal: 5,
                paddingVertical: 2,
                borderRadius: 10,
                position: 'absolute',
                bottom: '6%',
                left: '10%',
                right: '10%',
              }}>
              <Text
                style={{
                  ...FONTS.h3,
                  color: COLORS.gray,
                  textAlign: 'center',
                }}>
                {item.title}
              </Text>
              <Text
                style={{
                  ...FONTS.h6,
                  textAlign: 'center',
                  marginTop: SIZES.base,
                  color: COLORS.gray,
                }}>
                {item.description}
              </Text>
            </View>
          </View>
        );
      })}
    </Animated.ScrollView>
  );
}

function renderDots(scrollX) {
  const dotPosition = Animated.divide(scrollX, SIZES.width);

  return (
    <View style={styles.dotContainer}>
      {onBoardings.map((item, index) => {
        const opacity = dotPosition.interpolate({
          inputRange: [index - 1, index, index + 1],
          outputRange: [0.3, 1, 0.3],
          extrapolate: 'clamp',
        });

        const dotSize = dotPosition.interpolate({
          inputRange: [index - 1, index, index + 1],
          outputRange: [SIZES.base, 17, SIZES.base],
          extrapolate: 'clamp',
        });

        return (
          <Animated.View
            key={index}
            opacity={opacity}
            style={[styles.dot, {width: dotSize, height: dotSize}]}
          />
        );
      })}
    </View>
  );
}

const OnBoarding = () => {
  const [completed, setCompleted] = React.useState(false);

  const scrollX = new Animated.Value(0);
  React.useEffect(() => {
    scrollX.addListener(({value}) => {
      if (Math.floor(value / SIZES.width) === onBoardings.length - 1) {
        setCompleted(true);
      }
    });
    return () => scrollX.removeListener();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View>{renderBody(scrollX)}</View>
      <View style={styles.dotRootContainer}>{renderDots(scrollX)}</View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dotRootContainer: {
    position: 'absolute',
    bottom: '0.5%',
  },
  dotContainer: {
    flexDirection: 'row',
    padding: 5,
  },
  dot: {
    marginHorizontal: SIZES.radius,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.puprle,
  },
});

export default OnBoarding;
