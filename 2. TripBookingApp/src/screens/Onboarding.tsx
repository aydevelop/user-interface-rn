import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import icons from 'constants/icons';

const Onboarding = () => {
  return (
    <View>
      <Image
        source={icons.compass}
        resizeMode="contain"
        style={{
          width: 25,
          height: 25,
        }}
      />
    </View>
  );
};

export default Onboarding;

const styles = StyleSheet.create({});
