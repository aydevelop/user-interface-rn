import React from 'react';
import {StyleSheet, Text, View, Image, ColorPropType} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {COLORS} from '../constants/theme';
import icons from '../constants/icons';
import Home from 'screens/Home';

const Tab = createBottomTabNavigator();
const tabOptions = {
  showLabel: false,
  style: {
    height: '10%',
  },
};

const CameraButton = () => {
  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        width: 45,
        height: 45,
        borderRadius: 25,
        backgroundColor: COLORS.primary,
      }}>
      <Image
        source={icons.camera}
        resizeMethod="auto"
        style={{
          width: 25,
          height: 25,
        }}
      />
    </View>
  );
};

const Tabs = () => {
  return (
    <Tab.Navigator
      tabBarOptions={tabOptions}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused}) => {
          const tintColor = focused ? COLORS.primary : COLORS.gray;

          switch (route.name) {
            case 'Home':
              return (
                <Image
                  source={icons.flash}
                  resizeMethod="auto"
                  style={{tintColor: tintColor, width: 25, height: 25}}
                />
              );
            case 'Box':
              return (
                <Image
                  source={icons.cube}
                  resizeMethod="auto"
                  style={{tintColor: tintColor, width: 25, height: 25}}
                />
              );
            case 'Camera':
              return <CameraButton />;
            case 'Search':
              return (
                <Image
                  source={icons.search}
                  resizeMethod="auto"
                  style={{tintColor: tintColor, width: 25, height: 25}}
                />
              );
            case 'Favourite':
              return (
                <Image
                  source={icons.heart}
                  resizeMethod="auto"
                  style={{tintColor: tintColor, width: 25, height: 25}}
                />
              );
          }
        },
      })}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Box" component={Home} />
      <Tab.Screen name="Camera" component={Home} />
      <Tab.Screen name="Search" component={Home} />
      <Tab.Screen name="Favourite" component={Home} />
    </Tab.Navigator>
  );
};

export default Tabs;
