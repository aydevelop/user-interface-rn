import {COLORS, FONTS, SIZES} from 'constants/theme';
import icons from 'constants/icons';
import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import images from 'constants/images';
import {NavigationContainer} from '@react-navigation/native';

const Home = ({navigation}) => {
  const [newPlants, setNewPlants] = React.useState([
    {
      id: 0,
      name: 'Plant 1',
      img: images.plant1,
      favorite: false,
    },
    {
      id: 2,
      name: 'Plant 2',
      img: images.plant2,
      favorite: true,
    },
    {
      id: 3,
      name: 'Plant 3',
      img: images.plant3,
      favorite: false,
    },
    {
      id: 4,
      name: 'Plant 4',
      img: images.plant4,
      favorite: false,
    },
    {
      id: 5,
      name: 'Plant 5',
      img: images.plant5,
      favorite: false,
    },
    {
      id: 6,
      name: 'Plant 6',
      img: images.plant6,
      favorite: false,
    },
    {
      id: 7,
      name: 'Plant 7',
      img: images.plant7,
      favorite: false,
    },
  ]);

  const [friendList, setFriendList] = React.useState([
    {
      id: 0,
      img: images.profile1,
    },
    {
      id: 1,
      img: images.profile2,
    },
    {
      id: 2,
      img: images.profile3,
    },
  ]);

  function renderFriendsComponent() {
    return friendList.map((item, index) => (
      <View key={`index-${index}`} style={index === 0 ? {} : {marginLeft: -20}}>
        <Image
          source={item.img}
          resizeMode="cover"
          style={{
            width: 50,
            height: 50,
            borderRadius: 25,
            borderWidth: 3,
            borderColor: COLORS.primary,
          }}
        />
      </View>
    ));
  }

  function renderNewPlants(item, index) {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginHorizontal: SIZES.base,
        }}>
        <Image
          source={item.img}
          resizeMode="cover"
          style={{width: SIZES.width * 0.23, height: '75%', borderRadius: 15}}
        />

        <View
          style={{
            position: 'absolute',
            bottom: '20%',
            right: 0,
            backgroundColor: COLORS.primary,
            paddingHorizontal: SIZES.base,
            borderTopLeftRadius: 10,
            borderBottomLeftRadius: 10,
          }}>
          <Text style={{color: COLORS.white, ...FONTS.body5}}>{item.name}</Text>
        </View>

        <TouchableOpacity
          style={{position: 'absolute', top: '15%', left: 5}}
          onPress={() => {
            console.log('Favorite On Pressed');
          }}>
          <Image
            style={{
              width: 25,
              height: 25,
            }}
            source={item.favorite ? icons.heartRed : icons.heartGreenOutline}
          />
        </TouchableOpacity>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={{height: '30%', backgroundColor: COLORS.lightGray}}>
        <View
          style={{
            flex: 1,
            borderBottomLeftRadius: 0,
            borderBottomRightRadius: 0,
            backgroundColor: COLORS.primary,
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginTop: SIZES.padding / 2,
              marginHorizontal: SIZES.padding,
            }}>
            <Text style={{color: COLORS.white, ...FONTS.h2}}>New Plants</Text>

            <TouchableOpacity
              onPress={() => {
                console.log('Focus on password');
              }}>
              <Image
                source={icons.focus}
                resizeMethod="auto"
                style={{width: 25, height: 25}}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginTop: 0,
              marginHorizontal: 10,
            }}>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={newPlants}
              keyExtractor={item => item.name}
              renderItem={({item, index}) => renderNewPlants(item, index)}
            />
          </View>
        </View>
      </View>
      <View style={{height: '45%', backgroundColor: COLORS.gray}}>
        <View
          style={{
            flex: 1,
            borderBottomLeftRadius: 25,
            borderBottomRightRadius: 25,
            backgroundColor: COLORS.white,
          }}>
          <View>
            <View
              style={{
                marginTop: SIZES.font,
                marginHorizontal: SIZES.padding,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <Text style={{color: COLORS.black, ...FONTS.h2}}>
                  Today's Share
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    console.log('See all on pressed');
                  }}>
                  <Text style={{color: COLORS.secondary, ...FONTS.body3}}>
                    See All
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                height: '75%',
                marginTop: SIZES.base,
              }}>
              <View style={{flex: 1}}>
                <TouchableOpacity
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  onPress={() => {
                    navigation.navigate('PlantDetail');
                  }}>
                  <Image
                    source={images.plant5}
                    resizeMode="cover"
                    style={{
                      width: '90%',
                      height: '95%',
                      borderRadius: SIZES.radius,
                    }}></Image>
                </TouchableOpacity>
              </View>
              <View style={{flex: 1}}>
                <TouchableOpacity
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  onPress={() => {
                    navigation.navigate('PlantDetail');
                  }}>
                  <Image
                    source={images.plant5}
                    resizeMode="cover"
                    style={{
                      width: '90%',
                      height: '95%',
                      borderRadius: SIZES.radius,
                    }}></Image>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
      <View style={{height: '25%', backgroundColor: COLORS.gray}}>
        <View style={{flex: 1, backgroundColor: COLORS.gray}}>
          <View
            style={{
              marginTop: SIZES.radius,
              marginHorizontal: SIZES.padding,
            }}>
            <Text style={{color: COLORS.secondary, ...FONTS.h3}}>
              Added Friends - Total: {newPlants.length}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                height: '70%',
                marginTop: SIZES.base,
              }}>
              <View
                style={{
                  flex: 1.3,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                {renderFriendsComponent()}
                <Text style={{marginLeft: 5, ...FONTS.body3}}>+2 ...</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                }}>
                <Text>Add New</Text>
                <TouchableOpacity
                  onPress={() => {
                    console.log('add friend on pressed');
                  }}
                  style={{
                    marginLeft: SIZES.base,
                    width: 40,
                    height: 40,
                    borderRadius: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.lightGray,
                  }}>
                  <Image
                    source={icons.plus}
                    resizeMode="contain"
                    style={{width: 20, height: 20}}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
