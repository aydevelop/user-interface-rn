import icons from 'constants/icons';
import images from 'constants/images';
import {COLORS, FONTS, SIZES} from 'constants/theme';
import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

const ReqBar = ({icon, barPercentage}) => {
  return (
    <View style={{height: 60, alignItems: 'center'}}>
      <View
        style={{
          width: 50,
          height: 50,
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: 10,
          borderWidth: 1,
          borderColor: COLORS.black,
        }}>
        <Image
          source={icon}
          resizeMode="cover"
          style={{
            tintColor: COLORS.secondary,
            width: 30,
            height: 30,
          }}
        />
      </View>

      <View
        style={{
          position: 'absolute',
          width: '100%',
          bottom: 0,
          left: 0,
          right: 0,
          height: 3,
          marginTop: SIZES.base,
          backgroundColor: COLORS.gray,
        }}></View>

      <View
        style={{
          position: 'absolute',
          width: barPercentage,
          bottom: 0,
          left: 0,
          right: 0,
          height: 3,
          marginTop: SIZES.base,
          backgroundColor: COLORS.primary,
        }}></View>
    </View>
  );
};

const RegDetails = ({icon, label, detail}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
      }}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Image
          source={icon}
          resizeMode="cover"
          style={{
            tintColor: COLORS.secondary,
            width: 30,
            height: 30,
          }}
        />
        <Text
          style={{
            marginLeft: SIZES.base,
            color: COLORS.secondary,
            ...FONTS.h2,
          }}>
          {label}
        </Text>
      </View>
      <View>
        <Text
          style={{
            marginLeft: SIZES.base,
            color: COLORS.primary,
            ...FONTS.h2,
          }}>
          {detail}
        </Text>
      </View>
    </View>
  );
};

const PlanDetail = () => {
  function renderReqBar() {
    return (
      <View
        style={{
          flexDirection: 'row',
          marginTop: SIZES.padding,
          paddingHorizontal: SIZES.padding,
          justifyContent: 'space-between',
        }}>
        <ReqBar icon={icons.sun} barPercentage="50%" />
        <ReqBar icon={icons.drop} barPercentage="25%" />
        <ReqBar icon={icons.temperature} barPercentage="80%" />
        <ReqBar icon={icons.garden} barPercentage="30%" />
        <ReqBar icon={icons.seed} barPercentage="50%" />
      </View>
    );
  }

  function renderRequirements() {
    return (
      <View
        style={{
          flex: 1,
          marginTop: SIZES.padding,
          paddingHorizontal: SIZES.padding,
          justifyContent: 'space-around',
          paddingBottom: SIZES.padding,
        }}>
        <RegDetails icon={icons.sun} label="Sunlight" detail="15 °C" />
        <RegDetails icon={icons.drop} label="Water" detail="250 ML" />
        <RegDetails icon={icons.temperature} label="Room Temp" detail="25 °C" />
        <RegDetails icon={icons.garden} label="Soil" detail="3 Kg" />
        <RegDetails icon={icons.seed} label="Fertilizer" detail="120 Mg " />
      </View>
    );
  }

  function renderFooter() {
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            console.log('Take Action');
          }}
          style={{
            width: '75%',
            padding: SIZES.padding * 0.3,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            borderTopRightRadius: 30,
            borderBottomRightRadius: 30,
            backgroundColor: COLORS.primary,
          }}>
          <Text style={{color: COLORS.white, ...FONTS.h2}}>Take Action</Text>
          <Image
            source={icons.chevron}
            resizeMode="contain"
            style={{marginLeft: 5, width: 15, height: 15}}
          />
        </TouchableOpacity>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={{height: '30%'}}>
        <Image
          source={images.bannerBg}
          resizeMode="cover"
          style={{
            width: '100%',
            height: '100%',
          }}
        />
      </View>
      <View
        style={{
          flex: 1,
          backgroundColor: COLORS.lightGray,
          marginTop: -50,
          borderTopLeftRadius: 50,
          borderTopRightRadius: 50,
          paddingBottom: SIZES.padding * 2,
          paddingTop: SIZES.padding / 2,
        }}>
        <Text
          style={{
            paddingHorizontal: SIZES.padding,
            color: COLORS.secondary,
            textAlign: 'center',
            ...FONTS.h1,
          }}>
          Requirements
        </Text>
        {renderReqBar()}
        {renderRequirements()}
      </View>
      <View style={{position: 'absolute', bottom: SIZES.padding * 0.5}}>
        {renderFooter()}
      </View>
    </View>
  );
};

export default PlanDetail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
